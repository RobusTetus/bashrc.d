#!/bin/bash
js-sha512sum-check()
{
  local filename=$(grep path $1 | awk -F': ' '{print $2}')
  local hash=$(grep sha512 $1 | awk -F': ' '{print $2}' | uniq | base64 -d | xxd -p | tr -d \\n)
  echo "SHA512 (${filename}) = ${hash}" | sha512sum -c
}

js-sha256sum-check()
{
  local filename=$(grep path $1 | awk -F': ' '{print $2}')
  local hash=$(grep sha512 $1 | awk -F': ' '{print $2}' | uniq | base64 -d | xxd -p | tr -d \\n)
  echo "SHA256 (${filename}) = ${hash}" | sha256sum -c
}

alias lstemp="paste <(cat /sys/class/thermal/thermal_zone*/type) <(cat /sys/class/thermal/thermal_zone*/temp) | column -s $'\t' -t | sed 's/\(.\)..$/.\1°C/'"
alias lsflatpak="flatpak list --columns=application --app | tr '\n' ' ' && echo"

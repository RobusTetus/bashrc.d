#!/bin/bash
export PODMAN_COMPOSE_WARNING_LOGS=false
export EDITOR=vim
export PAGER=less
export HISTFILESIZE=10
export HISTSIZE=20


# Bash Shell bindings
bind 'TAB:menu-complete'
bind '"\e[Z":menu-complete-backward'
bind 'set show-all-if-ambiguous off'

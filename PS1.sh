#!/bin/bash

BG="\[\e[48;2;"
FG="\[\e[38;2;"

BOLD="\[\e[1m\]"

RST="\[\e[0m\]"
RSTBG="\[\e[49m\]"
RSTFG="\[\e[39m\]"

LIME="157;232;24"
PURPLE="116;70;230"
ORANGE="221;131;10"
BLUE="44;140;228"
BLACK="0;0;0"
WHITE="255;255;255"


USER_PROMPT="${BG}${LIME}m\]${FG}${BLACK}m\]  ${BOLD}\u ${RST}${FG}${LIME}m\]"

BAREMETAL_PROMPT="${BG}${PURPLE}m\]${FG}${WHITE}m\]  \w ${RSTBG}${FG}${PURPLE}m\]"
SSH_PROMPT="${BG}${ORANGE}m\]${FG}${BLACK}m\]  \w ${RSTBG}${FG}${ORANGE}m\]"
CONTAINER_PROMPT="${BG}${BLUE}m\]${FG}${WHITE}m\]  \w ${RSTBG}${FG}${BLUE}m\]"


if [[ -n $container ]]; then
  CURRENT_PROMPT=$CONTAINER_PROMPT
elif [[ -n $SSH_CLIENT ]]; then
  CURRENT_PROMPT=$SSH_PROMPT
else
  CURRENT_PROMPT=$BAREMETAL_PROMPT
fi


export PS1="${USER_PROMPT}${CURRENT_PROMPT}${RST} "
echo "PS1='${PS1}'" > ~/.ssh/environment
